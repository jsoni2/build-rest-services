package com.example.buildrestservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildRestServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuildRestServicesApplication.class, args);
	}

}
