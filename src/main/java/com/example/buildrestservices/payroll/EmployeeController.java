/**
 * 
 */
package com.example.buildrestservices.payroll;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author janak.soni
 *
 */

@RestController
public class EmployeeController {

	private final EmployeeRepository employeeRepository;

	private final EmployeeResourceAssembler employeeAssembler;

	/**
	 * @param employeeRepository
	 */
	public EmployeeController(EmployeeRepository employeeRepository,
			EmployeeResourceAssembler employeeAssembler) {
		this.employeeRepository = employeeRepository;
		this.employeeAssembler = employeeAssembler;	
	}

	// Get all the Employees
	@GetMapping("/employees")
	Resources<Resource<Employee>> all() {

		List<Resource<Employee>> employees = employeeRepository.findAll().stream()
				.map(employeeAssembler::toResource)
				.collect(Collectors.toList());

		return new Resources<>(employees,
				linkTo(methodOn(EmployeeController.class).all()).withSelfRel());
	}

	// Create new Employee
	@PostMapping("/employees")
	ResponseEntity<?> newEmployee(@RequestBody Employee newEmployee) throws URISyntaxException {

		Resource<Employee> resource = employeeAssembler.toResource(employeeRepository.save(newEmployee));

		return ResponseEntity
				.created(new URI(resource.getId().expand().getHref()))
				.body(resource);
	}

	// Get Single Employee
	@GetMapping("/employees/{id}")
	Resource<Employee> one(@PathVariable Long id) {

		Employee employee = employeeRepository.findById(id)
				.orElseThrow(() -> new EmployeeNotFoundException(id));

		return employeeAssembler.toResource(employee);
	}

	// Update Employee
	@PutMapping("/employees/{id}")
	ResponseEntity<?> replaceEmployee(@RequestBody Employee newEmployee, @PathVariable Long id) throws URISyntaxException {

	  Employee updatedEmployee = employeeRepository.findById(id)
	    .map(employee -> {
	      employee.setName(newEmployee.getName());
	      employee.setRole(newEmployee.getRole());
	      return employeeRepository.save(employee);
	    })
	    .orElseGet(() -> {
	      newEmployee.setId(id);
	      return employeeRepository.save(newEmployee);
	    });

	  Resource<Employee> resource = employeeAssembler.toResource(updatedEmployee);

	  return ResponseEntity
	    .created(new URI(resource.getId().expand().getHref()))
	    .body(resource);
	}

	// Delete an Employee
	@DeleteMapping("/employees/{id}")
	ResponseEntity<?> deleteEmployee(@PathVariable Long id) {
		employeeRepository.deleteById(id);
		return ResponseEntity.noContent().build();
	}

}
