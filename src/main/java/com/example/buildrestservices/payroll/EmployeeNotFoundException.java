/**
 * 
 */
package com.example.buildrestservices.payroll;

/**
 * @author janak.soni
 *
 */
public class EmployeeNotFoundException extends RuntimeException {
	
	EmployeeNotFoundException(Long id) {
	    super("Could not find employee " + id);
	  }
	
}
