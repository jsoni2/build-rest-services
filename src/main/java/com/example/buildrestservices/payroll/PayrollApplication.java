/**
 * 
 */
package com.example.buildrestservices.payroll;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author janak.soni
 *
 */

@SpringBootApplication
public class PayrollApplication {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(PayrollApplication.class, args);

	}

}
