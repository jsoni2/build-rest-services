/**
 * 
 */
package com.example.buildrestservices.payroll;

/**
 * @author janak.soni
 *
 */
public class OrderNotFoundException extends RuntimeException {
	
	OrderNotFoundException(Long id) {
	    super("Could not find order " + id);
	  }

}
