/**
 * 
 */
package com.example.buildrestservices.payroll;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author janak.soni
 *
 */
public interface OrderRepository extends JpaRepository<Order, Long> {

}
