/**
 * 
 */
package com.example.buildrestservices.payroll;

/**
 * @author janak.soni
 *
 */
public enum Status {
	
	IN_PROGRESS,
	COMPLETED,
	CANCELLED;

}
